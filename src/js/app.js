
(function () {
  objectFitImages();

  // burger-menu
  var isMobileMenuOpen = false;
  var mobileMenu = document.getElementById("mobile-menu");
  var hamburger = document.getElementById("hamburger");

  hamburger.addEventListener('click', function toggleMobileMenu() {
    this.classList.toggle("hamburger_active");
    if (this.classList.contains('hamburger_active')) {
      mobileMenu.style.display = 'flex';
    } else {
      mobileMenu.style.display = 'none';
    }
  })

  var mobileMenuLinks = mobileMenu.querySelectorAll(".mobile-menu__link");

  for (i = 0; i < mobileMenuLinks.length; i++) {
    mobileMenuLinks[i].addEventListener("click", function () {
      hamburger.classList.remove("hamburger_active");
      mobileMenu.style.display = 'none';
    });
  }

  // accordion
  if (window.innerWidth < 768) {
    var acc = document.getElementsByClassName("accordion");

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function () {
        this.classList.toggle("accordion_active");
        let panel = this.nextElementSibling;
        if (!this.classList.contains('accordion_active')) {
          panel.style.overflow = 'auto';
          panel.style.maxHeight = null;
        } else {
          panel.style.overflow = 'hidden';
          panel.style.maxHeight = '0px';
        }
      });
    }
  }

  // swiper
  const swiper = new Swiper('.swiper-container', {
    direction: 'horizontal',
    loop: false,
    simulateTouch: false,
    breakpoints: {
      320: {
        slidesPerView: 2,
        slidesPerGroup: 2,
        spaceBetween: 30,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
      },
      768: {
        slidesPerView: 2,
        slidesPerGroup: 1,
      },
      992: {
        slidesPerView: 3,
        slidesPerGroup: 1,
      },
      1100: {
        slidesPerView: 4,
        slidesPerGroup: 1,
      },
      1300: {
        slidesPerView: 5,
        slidesPerGroup: 1,
      }
    }
  });
})();
